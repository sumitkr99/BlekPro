﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BlekGame;

public class PlayerController : MonoBehaviour
{

	private List<Vector3> clickedPositionList;

	private Vector3 mouseDragPosition;
	private Vector3 mouseUpPosition;
	private Vector3 mouseDownPosition;

	private TrailRenderer myTrailRenderer;
	private Transform myTransfrom;

	private GameController controllerScript;
	private BoxCollider2D playerCollider;
	public GameObject burstParticle;

	// Use this for initialization
	void Start ()
	{
		controllerScript = GameObject.FindGameObjectWithTag (Constants.GameControllerTag).GetComponent<GameController> ();
		myTrailRenderer = GetComponent<TrailRenderer> ();
		myTransfrom = transform;
		playerCollider = GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		Debug.Log ("State name: " + GameManager.instance.CurrentGameState);
		if (Input.GetMouseButtonDown (0)) {
			
			if (GameManager.instance.CurrentGameState == GameState.PlayerMove) {
				print ("<color=green>Inside Down On Move</color>");
				StopAllCoroutines ();
				myTrailRenderer.time = 0f;
				myTrailRenderer.Clear ();
				GameManager.instance.CurrentGameState = GameState.StartedLevel;
				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
				//OnLevelFailed ();
//				GameManager.instance.numberOfCollectedDots = 0;
			}

			if (GameManager.instance.CurrentGameState == GameState.StartedLevel) {
				clickedPositionList = new List<Vector3> ();
				mouseDownPosition = GetWorldPosition (Input.mousePosition);
//				if (Vector3.Distance (mouseUpPosition, mouseDownPosition) > 1f) {
					
				myTrailRenderer.time = Constants.DefaultTrailTime;
//				}
//				myTrailRenderer.time = 0f;
				GameManager.instance.CurrentGameState = GameState.PlayerMove;
			}
		}

		if (Input.GetMouseButton (0)) {
			if (GameManager.instance.CurrentGameState == GameState.PlayerMove) {
				mouseDragPosition = GetWorldPosition (Input.mousePosition);
				myTransfrom.position = mouseDragPosition;
//				if (Vector3.Distance (mouseUpPosition, mouseDownPosition) > 0.5f) {
				clickedPositionList.Add (mouseDragPosition - mouseDownPosition);
//					gameObject.layer = 8;
//				}
			}
		}

		if (Input.GetMouseButtonUp (0)) {
			if (GameManager.instance.CurrentGameState == GameState.PlayerMove) {
				mouseUpPosition = GetWorldPosition (Input.mousePosition);
				if (Vector3.Distance (mouseUpPosition, mouseDownPosition) > 0.5f) {
					clickedPositionList.Add (mouseUpPosition - mouseDownPosition);
//					print ("<color=red>MOVING</color>");
					gameObject.layer = 8;
					StartCoroutine (Move (0));
				}
			}

		}
 		
	}

	/// <summary>
	/// Gets the world position.
	/// </summary>
	/// <returns>The world position.</returns>
	/// <param name="inputPosition">Input position.</param>
	private Vector3 GetWorldPosition (Vector3 inputPosition)
	{
		Vector3 worldPosition = Camera.main.ScreenToWorldPoint (inputPosition);
		worldPosition.z = myTransfrom.position.z;
		return worldPosition;
	}

	/// <summary>
	/// Move the specified i.
	/// </summary>
	/// <param name="i">The index.</param>
	IEnumerator Move (int i)
	{
		//Debug.Log ("Moveing.................... ");
		yield return new WaitForSeconds (Constants.MoveDelay);
		//myTrailRenderer.enabled = true;
		if (i < clickedPositionList.Count) {
			Vector3 tempPosition = mouseUpPosition + clickedPositionList [i];
			tempPosition.z = myTransfrom.position.z;
			myTransfrom.position = tempPosition;
			i++;
			StartCoroutine (Move (i));
		} else {
			if (BoundaryCheck ()) {
				OnLevelFailed ();
				yield break;	
			} 
			mouseUpPosition = myTransfrom.position;
			StartCoroutine (Move (0));
			yield break;
		}
	}

	/// <summary>
	/// Raises the trigger enter2 d event.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter2D (Collider2D other)
	{
		if (gameObject.layer == 8) {
			Debug.Log (other.name);
			if (other.tag.Equals (Constants.ColorTag)) {
				
				OnDotCollection (other.gameObject);
			} else if (other.tag.Equals (Constants.EndTag)) {
				StopAllCoroutines ();
				OnLevelFailed ();
			}
		}

		// For Magic Move::
//		if (other.name == "Magic") {
//			mouseUpPosition = Vector3.zero;
//			myTrailRenderer.enabled = false;
//			//clickedPositionList.Add (mouseUpPosition - other.transform.position);
//		}


		//



	}

	/// <summary>
	/// Raises the dot collection event.
	/// </summary>
	/// <param name="dotRenderer">Dot renderer.</param>
	void OnDotCollection (GameObject dotObject)
	{
		if (dotObject.layer == 9 ||dotObject.layer == 13 ) {
			OnLevelFailed ();
			return;
		} else {
			Instantiate (burstParticle, dotObject.transform.position, Quaternion.Euler (new Vector3 (-90, 0, 0)));
		}
//		dotObject.SetActive (false);
		OnObjectDisable(dotObject);
		if (GameManager.instance.IsLevelCompleted ()) {
			OnLevelCompletion ();
		} 
	}

	/// <summary>
	/// Raises the level completion event.
	/// </summary>
	public void OnLevelCompletion ()
	{
		Debug.Log ("<color=green>OnLevelCompletion</color>");
		StopAllCoroutines ();
		ResetPosition ();
		controllerScript.OnLevelCompletion ();
	}

	/// <summary>
	/// Raises the level failed event.
	/// </summary>
	public void OnLevelFailed ()
	{
		Debug.Log ("<color=red>OnLevelFailed</color>");
		StopAllCoroutines ();
		ResetPosition ();
		controllerScript.OnLevelFailure ();
	}

	/// <summary>
	/// Resets the position.
	/// </summary>
	public 	void ResetPosition ()
	{
		myTransfrom.position = Constants.DefaultPlayerPosition;
		myTrailRenderer.time = 0f;
	}

	public void OnObjectDisable(GameObject go)
	{
		go.GetComponent<SpriteRenderer> ().enabled = false;
		go.GetComponent<PolygonCollider2D> ().enabled = false;
	}

	/// <summary>
	/// Boundary check.
	/// </summary>
	/// <returns><c>true</c>, if transform position inside the boundary, <c>false</c> otherwise.</returns>
	bool BoundaryCheck ()
	{
		if ((myTransfrom.position.x < controllerScript.boundaryVectors [0].x) ||
		    (myTransfrom.position.x > controllerScript.boundaryVectors [1].x) ||
		    (myTransfrom.position.y < controllerScript.boundaryVectors [2].y) ||
		    (myTransfrom.position.y < controllerScript.boundaryVectors [3].x)) {
			return true;
		}
		return false;
	}
}
