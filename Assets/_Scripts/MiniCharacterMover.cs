﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniCharacterMover : MonoBehaviour
{
	public bool isRotate;
	public List<GameObject> miniCharObj = new List<GameObject> ();
	// Use this for initialization
	void Start ()
	{
		if (isRotate) {
			GetComponent<Rigidbody2D> ().AddTorque (10);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	//	void OnMouseDown ()
	//	{
	//		print ("On Mouse Down");
	//		onHit ();
	//	}


	void OnTriggerEnter2D (Collider2D other)
	{
		print ("Other Obj Name: " + other.name);
		onHit ();
	}

	public void onHit ()
	{
//		print (AngleBetweenTwoVector2 (transform.position, miniCharObj [0].transform.position));
//		forceApplier (miniCharObj [0], AngleBetweenTwoVector2 (transform.position, miniCharObj [0].transform.position));
//		forceApplier (miniCharObj [1], AngleBetweenTwoVector2 (transform.position, miniCharObj [1].transform.position));

		for (int i = 0; i < miniCharObj.Count; i++) {
			forceApplier (miniCharObj [i], AngleBetweenTwoVector2 (transform.position, miniCharObj [i].transform.position));
		}
	}


	public float AngleBetweenTwoVector2 (Vector2 vec1, Vector2 vec2)
	{
		Vector2 diference = vec2 - vec1;
		float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
		return Vector2.Angle (Vector2.right, diference) * sign;
	}

	public void forceApplier (GameObject go, float angle)
	{
		Vector3 dir = Quaternion.AngleAxis (angle, Vector3.forward) * Vector3.right;
		go.GetComponent<Rigidbody2D> ().AddForce (dir * 5, ForceMode2D.Impulse);
		go.layer = 12;
	}
}
