﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BlekGame;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

	public GameObject[] levels;
	public GameObject playerController;
	//	private int currentLevel;
	private GameObject currentLevelGameObject;

	public Transform[] screenEndTriggers;

	public GameObject uiPanel;
	public Text levelNumberText;

	public Vector3[] boundaryVectors;
	//store boundary world positions

	// Use this for initialization
	void Start ()
	{
		boundaryVectors = new Vector3[screenEndTriggers.Length];
		SetUpScreenEndTriggers ();
		SetUpLevel ();
	}

	/// <summary>
	/// Sets up level.
	/// </summary>
	void SetUpLevel ()
	{
//		Destroy(currentLevelGameObject);
//		currentLevelGameObject = (GameObject) GameObject.Instantiate(levels[GameManager.instance.CurrentLevel]);
//		GameManager.instance.OnStartGame(currentLevelGameObject.transform);
		playerController.layer = 0;
		levelNumberText.text = Constants.LevelText + (GameManager.instance.CurrentLevel + 1).ToString ();
		if (GameManager.instance.CurrentGameState == GameState.CompletedLevel) {
			uiPanel.SetActive (true);
			StartCoroutine (SetUpGame (Constants.RestartWaitTime));
		} else {
			uiPanel.SetActive (false);
			StartCoroutine (SetUpGame (0));
		}
		//StartCoroutine (SetUpGame (Constants.RestartWaitTime));
	}

	IEnumerator SetUpGame (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		uiPanel.SetActive (false);
		Destroy (currentLevelGameObject);
		currentLevelGameObject = (GameObject)GameObject.Instantiate (levels [GameManager.instance.CurrentLevel]);
		GameManager.instance.OnStartGame (currentLevelGameObject.transform);
	}

	/// <summary>
	/// Sets up screen end triggers.
	/// </summary>
	void SetUpScreenEndTriggers ()
	{
		Vector3 pos;
		for (int i = 0; i < 4; i++) {
			pos = screenEndTriggers [i].position;
			boundaryVectors [i] = GetPos (i);
			if (i < 2)
				pos.x = boundaryVectors [i].x;
			else
				pos.y = boundaryVectors [i].y;
	
			screenEndTriggers [i].position = pos;
		}
	}

	/// <summary>
	/// Gets the position.
	/// </summary>
	/// <returns>The position.</returns>
	/// <param name="id">Identifier.</param>
	private Vector3 GetPos (int id)
	{
		switch (id) {
		case 0:
			return Camera.main.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f));
		case 1:
			return Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f));
		case 2:
			return Camera.main.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f));
		case 3:
			return Camera.main.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f));
		default:
			return Vector3.zero;
		}
	}

	/// <summary>
	/// Raises the level completion event.
	/// </summary>
	public void OnLevelCompletion ()
	{
		GameManager.instance.CurrentGameState = GameState.CompletedLevel;
		onNextLevel ();
//		GameManager.instance.CurrentLevel++;
//		if (GameManager.instance.CurrentLevel >= levels.Length)
//			GameManager.instance.CurrentLevel = 0;
//		
//		SetUpLevel ();
	}

	/// <summary>
	/// Raises the level failure event.
	/// </summary>
	public void OnLevelFailure ()
	{
		//GameOver
		GameManager.instance.CurrentGameState = GameState.GameOver;
		StartCoroutine (RestartGame ());
	}

	/// <summary>
	/// Restarts the game after the delay.
	/// </summary>
	IEnumerator RestartGame ()
	{
		yield return new WaitForSeconds (Constants.RestartWaitTime);
		SetUpLevel ();
	}
		
	public void onNextLevel()
	{
		GameManager.instance.CurrentLevel++;
		if (GameManager.instance.CurrentLevel >= levels.Length)
			GameManager.instance.CurrentLevel = 0;

		SetUpLevel ();
	}
	public void onPreviousLevel()
	{
		GameManager.instance.CurrentLevel--;
		if (GameManager.instance.CurrentLevel <0)
			GameManager.instance.CurrentLevel = 0;

		SetUpLevel ();
	}
	
}
