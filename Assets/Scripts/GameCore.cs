﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Blek Game.
/// </summary>
namespace BlekGame {

	public enum GameState{NotStarted, StartedLevel, PlayerMove, CompletedLevel, GameOver}
	
	/// <summary>
	/// Game manager.
	/// </summary>
	public class GameManager {
		private static GameManager gmInstance;

		private GameState currentGameState;	//the current game state

		private int currentLevel;	//current level of the game

		public int numberOfCollectedDots;	//no of collected dots

		private int numberOfDots;	//no of dots in the level


		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <value>The instance.</value>
		public static GameManager instance {
			get {
				if(gmInstance == null)
					gmInstance = new GameManager();
				return gmInstance;
			}
		}

		private GameManager() {
			//initalize
			CurrentGameState = GameState.NotStarted;
			CurrentLevel = 0;
		}

		/// <summary>
		/// Gets or sets the state of the current game.
		/// </summary>
		/// <value>The state of the current game.</value>
		public GameState CurrentGameState {
			get {
				return currentGameState;
			} set {
				currentGameState = value;
			}
		}

		/// <summary>
		/// Gets or sets the current level.
		/// </summary>
		/// <value>The current level.</value>
		public int CurrentLevel {
			get {
				return currentLevel;
			} set {
				currentLevel = value;
			}
		}

		/// <summary>
		/// Raises the start game event.
		/// </summary>
		public void OnStartGame(Transform currentLevelObject) {
			numberOfCollectedDots = 0;
			numberOfDots = GetDotCount(currentLevelObject);
			GameManager.instance.CurrentGameState = GameState.StartedLevel;
			Debug.Log ("No Of Dots=  " + numberOfDots);
		}

		/// <summary>
		/// Gets the dot count.
		/// </summary>
		/// <returns>The dot count.</returns>
		/// <param name="currentLevelObject">Current level object.</param>
		private int GetDotCount(Transform currentLevelObject) {
			int count=0;
			foreach(Transform t in currentLevelObject) {
//				if(t.GetComponent<SpriteRenderer>().color != Color.black) {
//					count++;
//				}
				if(t.gameObject.layer== 10 || t.gameObject.layer== 13) {
					count++;
				}
				t.gameObject.SetActive(true);
			}
			return count;
		}

		/// <summary>
		/// Determines whether this instance is level completed.
		/// </summary>
		/// <returns><c>true</c> if this instance is level completed; otherwise, <c>false</c>.</returns>
		public bool IsLevelCompleted() {
			numberOfCollectedDots++;
			return (numberOfDots == numberOfCollectedDots);
		}

	}

	/// <summary>
	/// Constants.
	/// </summary>
	public class Constants {
		public const string GameControllerTag = "GameController";
		public const string EndTag = "End";
		public const string ColorTag = "Color";
		public const float RestartWaitTime = 0.75f;
		public static Vector3 DefaultPlayerPosition = new Vector3(100f, 100f, 0f);
		public const float DefaultTrailTime = 0.75f;
		public const float MoveDelay = 0.01f;
		public const string LevelText = "Level ";
	}

}