﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BlekGame;

public class MiniCharacterScript : MonoBehaviour
{
	private PlayerController playerCtlr;
	private GameController gameCtlr;
	public GameObject burstParticle;
	// Use this for initialization
	void Start ()
	{

		playerCtlr = GameObject.Find ("PlayerController").GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}


	void OnTriggerEnter2D (Collider2D other)
	{
		print ("Other Obj Name:XXXXXXXXXXXXXXXXXX " + other.name);
		if (other.gameObject.layer == 10 || other.gameObject.layer == 13) {
			//other.gameObject.SetActive (false);
			ObjDisabler(other.gameObject);
			Instantiate (burstParticle, transform.position, Quaternion.Euler (new Vector3 (-90, 0, 0)));
			if (GameManager.instance.IsLevelCompleted ()) {
				playerCtlr.OnLevelCompletion ();
			} 
		}
	}
	void ObjDisabler(GameObject go)
	{
		go.GetComponent<SpriteRenderer> ().enabled = false;
		go.GetComponent<CircleCollider2D> ().enabled = false;
	}

}
