﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMoveTowards : MonoBehaviour
{
	public Transform target;
	public float speed;
	private PlayerController playerCtlr;
	// Use this for initialization
	void Start ()
	{
		playerCtlr = GameObject.Find ("PlayerController").GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, target.position, step);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		print ("Other Obj Name: " + other.name);
		if (other.gameObject.layer == 10) {
			onHitTarget ();
		}
	}

	private void onHitTarget ()
	{
		playerCtlr.OnLevelFailed ();
	}
}
